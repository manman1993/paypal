<?php 
// Redirect to the home page if id parameter not found in URL 
if(empty($_GET['id'])){ 
    header("Location: index.php"); 
} 
 
 
// Include and initialize paypal class 
include_once 'PaypalExpress.class.php'; 
$paypal = new PaypalExpress; 

 

?>

<div class="item">
   
    
    <!-- Checkout button -->
    <div id="paypal-button"></div>
</div>
<script src="https://www.paypalobjects.com/api/checkout.js"></script>