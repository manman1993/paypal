

<div class="item">
    <a href="checkout.php?id=10">BUY</a>
</div>




<div id="paypal-button"></div>

<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<!--
JavaScript code to render PayPal checkout button and execute payment
-->
<script>
paypal.Button.render({
    // Configure environment
    env: 'sandbox',
    client: {
        sandbox: 'ASHXLg6XEKwoyU1o8FtkMyKsMwIMcmq7kldWC6u2Z6jaEoaKsUUy7aZoM5Q3n0MSDjVGqnHohJYUjZto',
        production: ''
    },
    // Customize button (optional)
    locale: 'en_US',
    style: {
        size: 'small',
        color: 'gold',
        shape: 'pill',
    },
    // Set up a payment
    payment: function (data, actions) {
        return actions.payment.create({
            transactions: [{
                amount: {
                    total: '1',
                    currency: 'usd'
                }
            }]
      });
    },
    // Execute the payment
    onAuthorize: function (data, actions) {
        return actions.payment.execute()
        .then(function () {
            // Show a confirmation message to the buyer
            //window.alert('Thank you for your purchase!');
            
            // Redirect to the payment process page
            window.location = "process.php?paymentID="+data.paymentID+"&token="+data.paymentToken+"&payerID="+data.payerID+"&pid=<?php echo $productData['id']; ?>";
        });
    }
}, '#paypal-button');
</script>